FROM mariadb:latest
ENV MYSQL_ROOT_PASSWORD "root"
ENV MYSQL_DATABASE thermopi
ENV MYSQL_USER thermopi
ENV MYSQL_PASSWORD thermopi
ADD schema.sql /docker-entrypoint-initdb.d
EXPOSE 3306
