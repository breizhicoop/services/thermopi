# ThermoPi

# ThermoPi Dev

A bunch of python scripts to:
- report temperatures and store them in db
- send mail in case of alert
- purge db from old temperatures report (TODO)

# Deploy

Initial setup:
* python3 -m venv venv_deploy
* source venv_deploy/bin/activate
* python -m pip install -r requirements_deploy.txt # install fabric and dotenv

Run the fabric file to deploy:
* fab deploy -t # test deploy eg. => cfg/settings_deploy_test.env
* fab deploy # prod deploy eg. => cfg/settings_deploy.env

Note:
* copy files (python, env, config(s)) + create & update python virtual env

# Config

* probe_alert_threshold 
* probe_alert_duration

# Env

## settings.env (or settings_test.env)

For everything in Scripts folder
 
* SMTP_ADDR_TO="foo@gmail.com,bar@gmail.com" # only 1 mail is ok too
* SMTP_BCC_TO="foo@gmail.com,bar@gmail.com" # only 1 mail is ok too

## settings_deploy.env (or settings_deploy_test.env)

For fabric deploy script

# Run

Initial setup:
* python3 -m venv venv
* source venv/bin/activate
* python -m pip install -r requirements.txt

## pi_purge

* python Scripts/pi_purge.py --threshold 10 --dry_run
* python Scripts/pi_purge.py --threshold 10 --test --dry_run  # load cfg/config_test.json && settings_test.env

