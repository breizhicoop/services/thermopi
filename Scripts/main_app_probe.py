#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from datetime import datetime, timedelta
import csv
import socket
import os.path
import sys
import os

from Scripts.parser_config_json import ConfigParser
from Scripts.pi_temperature import ProbeTemp
from Scripts.pi_mariadb import Mysql
from cfg import settings


def find_probe_all_temp(date, save_path_temperature):
    """
        Find all datas register for a probe into files after "date"
        :param obj date: a datetime object which is the last entry date and time for a thermo pi.
        :param str save_path_temperature: directory path where temperature are saved.
        :return: a list of datas (temperature readings) to register into database.
    """
    date_range_path = list()
    datas = list()

    # Find path to check.
    # Not very useful (star and end) but we could use it later to improve the injection behaviour in database
    start = date
    end = datetime.now()

    #  We inject in the database all the lines in the .txt files (day file). Because the duplicate entry are not insert
    #  in the mariadb.
    #  Convert the datetime object (with date and hours:min:sec) to a date object (with date)
    #  and format years,month,day to check the diff in days and not in hours. Case: start = "2020-07-04 23:55:02" and
    #  end = "2020-07-05 00:00:02"
    for x in range(0, (end.date() - start.date()).days + 1):
        date_range = start + timedelta(days=x)
        date_path = os.path.join(save_path_temperature,
                                 str(date_range.year),
                                 str(date_range.date()) + '.txt'
                                 )
        # if file exists os.path.exists():
        if os.path.isfile(date_path):
            date_range_path.append(date_path)

    for file in date_range_path:
        with open(file, 'r') as f:
            reader = csv.reader(f)
            headers = next(f)
            for row in reader:
                datas.append(tuple(row))

    return datas


if __name__ == '__main__':

    print("-----------------------------------------------------")
    print("----   Start Script {!s}   ----".format(datetime.now()))
    print("-----------------------------------------------------")

    # Get config from configuration file
    all_config = ConfigParser(settings.DEFAULT_DEVICE_CONFIG_PATH)
    # Update configuration infos with detected probes.
    if not all_config.update_probe_spec():
        print("Update probe configuration failed - end of script -")
        sys.exit()

    # #######################################################
    # Get and save temperature for all detected probes
    # #######################################################
    print('Get temperature for each probes and save them into a csv file.')
    # TODO verification que l'on a bien des specifications.
    for probe_spec in all_config.probes.get("specifications"):
        # For each detected probe get temperature
        probe_temp = ProbeTemp(probe_spec.get("probe_id", "Probe_id not found"),
                               probe_spec.get("probe_name", "Probe_name not found"),
                               probe_spec.get("device_path", "Device_path not found"),
                               save_path_temperature=all_config.probes.get("save_path_temperature"))
        # Get probe temp and save its temperature into a file
        probe_temp.get_temp_and_save()

    # #######################################################
    # Save temperature into database
    # #######################################################
    print('Saving temperatures into database !')

    # Init database object
    mysql = Mysql(login=settings.MYSQL_LOGIN,
                  password=settings.MYSQL_PASSWORD,
                  host=settings.MYSQL_HOST,
                  port=settings.MYSQL_PORT,
                  database_name=settings.MYSQL_NAME)

    # Connect to database
    mysql.login_database()

    # for probe_spec in all_config.probes.get("specifications"):
    # Build query
    pi_hostname = socket.gethostname()
    # probe_name = probe_spec.get("probe_name")
    temperature_path = all_config.probes.get("save_path_temperature")
    query = 'select datetime from tempreading where hostname="' + \
            pi_hostname + \
            '" order by datetime desc limit 1;'

    result = mysql.get_query_result(query)

    # Get last timestamp or entry into database and table "tempreading" for a probe
    # First time, nothing found probe_last_date is empty
    if not result:
        print('Query result: no entry in database for "{!s}" .'.format(pi_hostname))
        probe_last_date = datetime(2019, 4, 1)
    else:
        print('Query result: last entry in database for "{!s}" was at "{!s}"'.format(pi_hostname, result[0][0]))
        probe_last_date = result[0][0]

    # Build a list of line to insert into database
    save_datas_list = find_probe_all_temp(probe_last_date, temperature_path)

    mysql.insert_temp('tempreading', save_datas_list)
    # mysql.query('select * from tempreading;')
    mysql.query('select * from tempreading order by datetime desc limit 10;')

    # Close connection to database
    mysql.close()

    print("-----------------------------------------------------")
    print("----   End Script {!s}     ----".format(datetime.now()))
    print("-----------------------------------------------------")

