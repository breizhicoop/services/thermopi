#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import sys
import os
from cfg import settings
import json
import socket


class ConfigParser:
    """
        ConfigParser class. Parse config file .
    """
    def __init__(self, config_path):
        """
            Parse a config file which described usefull infos (smtp, database, probe confs ..).
            :param str config_path: full path of the configuration file.
            :return: the created object.
        """

        if not os.path.exists(config_path):
            print("this config_path does not exists: {!s}".format(config_path))
            sys.exit()
        config = self.parse_conf_file(config_path)
        self.version = config.get("version", "Unknown")
        self.config_path = config_path
        # TODO verification si vide ou pas.
        # TODO a voir si on passe pas tout en object à partir des classes ProbeTemp, Mysql, Smtp
        self.probes = config.get("probes")
        self.smtp = config.get("smtp")
        self.mariadb = config.get("mariadb")

    @staticmethod
    def parse_conf_file(config_path):
        """
            Open config file and parse json files to a python dictionary.
            :param str config_path: full path to json config files.
            :return dict dict_datas: None if there are errors or a datas dictionary with all config file infos.
        """

        # Open logging.json config file.
        with open(config_path) as cfg_file:
            # Read json config file and convert it to a string.
            str_datas = cfg_file.read()
            try:
                # Decoding json or deserialize str_datas to dictionary.
                dict_datas = json.loads(str_datas)
            except Exception as e:
                print('Error, can not serialize "{!s}" config file. Check its format.'.format(config_path))
                print('Error, -- CAUSE -- {!s}!'.format(e))
                return None

            return dict_datas

    def update_probe_spec(self):
        """
            Update the probe specifications with only useful infos used by the local raspberry pi and locals probes
            :return: True if everything was OK, False otherwise.
        """
        from Scripts.pi_temperature import detect_all_probes

        list_probes_found = list()
        status = True

        # Build the full path directory where the temperatures readings will be stored.
        self.probes["save_path_temperature"] = os.path.join(settings.WORK_PATH,
                                                            self.probes.get("temperature_dir", "temperature"))

        # # Update probe specifications with "device_path". Example "device_path" =
        # # "/sys/bus/w1/devices/28-011453680eaa/w1_slave"
        # for probe_specifications in self.probes.get("specifications"):
        #     # Build probe device_path (file path used to read the temperature. For each probe, the path is unique)
        #     probe_specifications["device_path"] = os.path.join(os.path.dirname(self.probes.get("probes_hard_path")),
        #                                                        probe_specifications.get("probe_id"),
        #                                                        self.probes.get("probes_id_temp_file"))

        # Detect all the probes used on this local pi
        detected_probes_list = detect_all_probes(self.probes.get("probes_hard_path"))

        if len(detected_probes_list) == 0:
            print('No probe detected for "{!s}" !'.format(socket.gethostname()))
            status = False
            return status

        for probe_id in detected_probes_list:
            # Find probe specification into config file with probe_id
            probe_spec = self.find_probe_spec(probe_id)
            if probe_spec:
                # we found it
                # Add device_path infos
                list_probes_found.append(probe_spec)
            else:
                print('A probe with id "{!s}" has been detected but no informations about it has been found in '
                      'config file "{!s}" !'.format(probe_id, self.config_path))
                status = False

        if status:
            self.probes["specifications"] = list_probes_found
        else:
            print('Missing infos into json configuration files about probes specifications !')
            return status

        # Update probe specifications with "device_path". Example "device_path" =
        # "/sys/bus/w1/devices/28-011453680eaa/w1_slave"
        for probe_specifications in self.probes.get("specifications"):
            # Build probe device_path (file path used to read the temperature. For each probe, the path is unique)
            probe_specifications["device_path"] = os.path.join(os.path.dirname(self.probes.get("probes_hard_path")),
                                                               probe_specifications.get("probe_id"),
                                                               self.probes.get("probes_id_temp_file"))

        return status

    def find_probe_spec(self, probe_id):
        """
            Find probe specifications (name, etc, ...
            :param str probe_id: probe id to look for.
            :return: a dictionary with probe specifications or None if nothing found.
        """
        for specification in self.probes.get("specifications"):
            if specification.get("probe_id") == probe_id:
                # We found the spec
                return specification
        else:
            print('No specification found for probe with id {!s} !'.format(probe_id))
            return None


if __name__ == '__main__':
    config = ConfigParser(settings.DEFAULT_DEVICE_CONFIG_PATH)
    config.update_probe_spec()
    pass