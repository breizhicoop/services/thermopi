#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from pathlib import Path
import os
import datetime
import csv
import sys
import argparse
from typing import Dict

from dotenv import load_dotenv

sys.path.append('./')
from Scripts.parser_config_json import ConfigParser
from Scripts.pi_smtp import Smtp
from Scripts.pi_mariadb import Mysql
from cfg import settings


SUBJECTS: Dict[str, str] = {
    "too_high": "Breizhicoop - ThermoPi - {sondename}: Seuil température dépassé.",
    "not_working": "Breizhicoop - ThermoPi - {sondename}: Sonde déconnectée",
    "working_again": "Breizhicoop - ThermoPi - {sondename}: Sonde reconnectée",
    "temp_ok": "Breizhicoop - Thermopi- {sondename}: Seuil température normal.",
}

MESSAGES: Dict[str, str] = {
    "too_high": "La sonde {sondename} a dépassé le seuil de : "
                "{alert_threshold}° pendant {duration} min.\n"
                "Email envoyé le : {now}",
    "not_working": "La sonde {sondename} est déconnectée depuis {duration} min.\n"
                   "Email envoyé le : {now}",
    "working_again": "La sonde {sondename} est reconnectée.\n"
                     "Email envoyé le : {now}",
    "temp_ok": "La température de la sonde {sondename} est de retour à une valeur normale.\n"
               "Email envoyé le : {now}",
}


def send_mail(smtp_server, mail_from, mail_to, subject, msg, bcc_to=None):
    smtp_server.connect_server_smtp()
    smtp_server.send_email(from_addr=mail_from,
                           to_addr=mail_to,
                           subject=subject,
                           message=msg,
                           to_bcc=bcc_to)
    smtp_server.close()


def alert(args):
    all_config = ConfigParser(settings.DEFAULT_DEVICE_CONFIG_PATH)

    # test = True # temp var to enable testing purpose

    #
    # read config and get path where temperature files are saved

    path_temperature = Path("temperature")

    if args.test:
        env_path = Path("cfg") / "settings_test.env"
    else:
        env_path = Path("cfg") / "settings_prod.env"

    load_dotenv(dotenv_path=env_path, verbose=True)

    SMTP_USER = os.getenv("SMTP_USER")
    SMTP_PASSWORD = os.getenv("SMTP_PASSWORD")
    SMTP_HOST = os.getenv("SMTP_HOST")
    SMTP_PORT = os.getenv("SMTP_PORT")
    SMTP_ADDR_FROM = os.getenv("SMTP_ADDR_FROM")
    SMTP_ADDR_TO = os.getenv("SMTP_ADDR_TO").split(",")
    SMTP_BCC_TO = os.environ["SMTP_BCC_TO"].split(",") if "SMTP_BCC_TO" in os.environ else None
    MYSQL_LOGIN = os.getenv("MYSQL_LOGIN")
    MYSQL_PASSWORD = os.getenv("MYSQL_PASSWORD")
    MYSQL_HOST = os.getenv("MYSQL_HOST")
    MYSQL_PORT = os.getenv("MYSQL_PORT")
    MYSQL_NAME = os.getenv("MYSQL_NAME")
    # print("SMTP ADDR", SMTP_ADDR_FROM, SMTP_ADDR_TO)

    # Init Smtp object
    smtp_server = Smtp(smtp_address=SMTP_HOST, port=SMTP_PORT,
                       user=SMTP_USER, password=SMTP_PASSWORD)

    #
    # build csv file name
    # today and yesterday files
    today = datetime.date.today()
    print("date today", today)
    # Note: csv file are historically saved using the .txt extension (and not .csv)

    if args.test:
        csv_temp_today = path_temperature / "2020" / "2020-09-10.txt"
        csv_temp_yesterday = path_temperature / "2020" / "2020-09-09.txt"
        now = datetime.datetime(year=2020, month=9, day=10, hour=9, minute=28)
    else:
        csv_temp_today = path_temperature / str(today.year) / ("%s.txt" % today.strftime("%Y-%m-%d"))
        yesterday = today - datetime.timedelta(days=1)
        print("date yesterday", yesterday)
        csv_temp_yesterday = path_temperature / str(yesterday.year) / ("%s.txt" % yesterday.strftime("%Y-%m-%d"))
        now = datetime.datetime.now()

    # parse csv and extract information

    data_temp = {}

    with open(csv_temp_yesterday) as csv1:
        reader = csv.DictReader(csv1)
        for row in reader:
            data_temp.setdefault(row["sondename"], [])
            data_temp[row["sondename"]].append(row)

    with open(csv_temp_today) as csv2:
        reader = csv.DictReader(csv2)
        for row in reader:
            data_temp.setdefault(row["sondename"], [])
            data_temp[row["sondename"]].append(row)

    mysql = Mysql(login=MYSQL_LOGIN,
                  password=MYSQL_PASSWORD,
                  host=MYSQL_HOST,
                  port=MYSQL_PORT,
                  database_name=MYSQL_NAME
                  )
    mysql.login_database()

    # iter per sondename and check if an alert should be send
    mail_now_ = datetime.datetime.now()
    mail_now: str = mail_now_.strftime("%d %B %Y à %Hh%M")

    print("=======================")

    for sondename, data_per_sondename in data_temp.items():
        for probe_spec in all_config.probes.get("specifications"):
            now_duration = now - datetime.timedelta(minutes=probe_spec["probe_alert_duration"])
            if sondename == probe_spec["probe_name"]:
                print("sondename =====>", sondename)
                data_filtered = [d for d in data_per_sondename if
                                 datetime.datetime.strptime(d["datetime"], "%Y-%m-%d %H:%M:%S.%f") > now_duration]
                print("data_filtered ===>", data_filtered)
                query_alert_working = 'SELECT alert_working FROM temp_alerte WHERE sondename="' + \
                                      sondename + \
                                      '";'
                result_alert_working = mysql.get_query_result(query_alert_working)
                alert_working = result_alert_working[0][0]
                if data_filtered == [] and not alert_working:
                    print("send-email-sondename-notworking")
                    # send_email_sondename_not_working(smtp_server, sondename,
                    #                                 probe_spec['probe_alert_duration'], mail_now)
                    sub = SUBJECTS["not_working"].format(sondename=sondename)
                    msg = MESSAGES["not_working"].format(sondename=sondename,
                                                         duration=probe_spec["probe_alert_duration"],
                                                         now=mail_now)
                    send_mail(smtp_server, SMTP_ADDR_FROM, SMTP_ADDR_TO, sub, msg, SMTP_BCC_TO)

                    set_alert_working_true = 'UPDATE temp_alerte SET alert_working = TRUE WHERE sondename = "%s";' % (
                        sondename)
                    mysql.cursor.execute(set_alert_working_true)
                    mysql.db.commit()
                elif data_filtered and alert_working:
                    # send_email_sondename_working(smtp_server, sondename, mail_now)
                    sub = SUBJECTS["working_again"].format(sondename=sondename)
                    msg = MESSAGES["working_again"].format(sondename=sondename,
                                                           now=mail_now)
                    send_mail(smtp_server, SMTP_ADDR_FROM, SMTP_ADDR_TO, sub, msg, SMTP_BCC_TO)
                    set_alert_working_false = 'UPDATE temp_alerte SET alert_working = FALSE WHERE sondename = "%s";' % (
                        sondename)
                    mysql.cursor.execute(set_alert_working_false)
                    mysql.db.commit()
                else:
                    l = [bool(float(data["temperature"]) > probe_spec["probe_alert_threshold"]) for data in
                         data_filtered]
                    print(l)
                    query_alert_temp = 'SELECT alert_temperature FROM temp_alerte WHERE sondename="' + \
                                       sondename + \
                                       '";'
                    result_alert_temp = mysql.get_query_result(query_alert_temp)
                    alert_temp = result_alert_temp[0][0]
                    if l and all(l) and not alert_temp:
                        # send an alert (email?)
                        print("mail ----------- alert--------")
                        # send_email_temp_high(smtp_server, sondename, probe_spec['probe_alert_threshold'],
                        #                     probe_spec['probe_alert_duration'], mail_now)
                        sub = SUBJECTS["too_high"].format(sondename=sondename)
                        msg = MESSAGES["too_high"].format(sondename=sondename,
                                                          alert_threshold=probe_spec["probe_alert_threshold"],
                                                          duration=probe_spec["probe_alert_duration"],
                                                          now=mail_now)
                        send_mail(smtp_server, SMTP_ADDR_FROM, SMTP_ADDR_TO, sub, msg, SMTP_BCC_TO)
                        set_temp_alerte_true = 'UPDATE temp_alerte SET alert_temperature = TRUE WHERE sondename = "%s";' % (
                            sondename)
                        mysql.cursor.execute(set_temp_alerte_true)
                        mysql.db.commit()
                    elif l and not all(l) and alert_temp:
                        # send_email_good_temp(smtp_server, sondename, mail_now)
                        sub = SUBJECTS["temp_ok"].format(sondename=sondename)
                        msg = MESSAGES["temp_ok"].format(sondename=sondename,
                                                         now=mail_now)
                        send_mail(smtp_server, SMTP_ADDR_FROM, SMTP_ADDR_TO, sub, msg, SMTP_BCC_TO)
                        set_temp_alert_false = 'UPDATE temp_alerte SET alert_temperature = FALSE WHERE sondename = "%s";' % (
                            sondename)
                        mysql.cursor.execute(set_temp_alert_false)
                        mysql.db.commit()
        print("===")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--test", action="store_true")
    args_ = parser.parse_args()

    alert(args_)
