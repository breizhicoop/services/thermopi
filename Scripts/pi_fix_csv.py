#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import argparse
from pathlib import Path
import shutil
import csv

from Scripts.parser_config_json import ConfigParser
from cfg import settings


def fix_csv(f):

    shutil.copy2(f, str(f) + ".bak")
    # reader = csv.reader(x.replace('\0', '') for x in mycsv)

    with open(f) as csv_fp:
        reader = csv.reader(x.replace("\0", "") for x in csv_fp)
        with open(str(f) + ".fix", "w") as csv_fp2:
            csv_writer = csv.writer(csv_fp2, dialect="excel")
            for row in reader:
                csv_writer.writerow(row)

    # overwrite original file with fixed file
    shutil.copy2(str(f) + ".fix", f)


def check_csv(args):

    all_config = ConfigParser(settings.DEFAULT_DEVICE_CONFIG_PATH)

    # test = True # temp var to enable testing purpose

    #
    # read config and get path where temperature files are saved

    path_temperature = Path("temperature")
    # path_temperature = Path("debug_null_byte")
    # path_temperature = Path("tests/temperature")
    print("path temperature", path_temperature)

    for folder in path_temperature.iterdir():
        for csv_file in folder.glob("%s-*.txt" % folder.name):
            print("Checking csv: %s" % csv_file)
            if "\0" in open(csv_file).read():
                if args.dryrun:
                    print("Found null bytes in", csv_file)
                else:
                    fix_csv(csv_file)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--dryrun", action="store_true")
    args_ = parser.parse_args()

    check_csv(args_)
