#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import mysql.connector as mariadb
from tabulate import tabulate
from datetime import datetime

import os
from pathlib import Path

from dotenv import load_dotenv

# TODO check if we should used MySQLConnection
from mysql.connector import MySQLConnection, Error

# TODO cursor = connection.cursor(buffered=True) -> check cest quoi buffered=True
# TODO replace fetchall() by fetchmany() or by fetchone(). fetchmany() is better
# cursor.fetchall() to fetch all rows .
# cursor.fetchone() to fetch single row.
# cursor.fetchmany(SIZE) to fetch limited rows.
# TODO add docstrings
# TODO ajouter les loggings
# TODO fichier de config json pour la base de données
# TODO Delete or save all past temp (3 month ago) to avoid to full the database and query become too long


# ######################################################################
class Mysql:

    def __init__(self, login, password, host, port, database_name):
        self.__login = login
        self.__password = password
        self.__host = host
        self.__port = port
        self.__database_name = database_name
        # Database object. It should be not None when it is connected
        self.db = None
        # Init cursor
        self.cursor = None

    def login_database(self):
        # TODO mettre un timeout
        try:
            print('Connecting to "{!s}" mariadb and database "{!s}" ...'.format(self.__host, self.__database_name))
            self.db = mariadb.connect(user=self.__login,
                                      password=self.__password,
                                      host=self.__host,
                                      port=self.__port,
                                      database=self.__database_name)

            if self.db.is_connected():
                print('Connection established.')
                # Init the cursor object after connecting to database to be able to launch query
                self.cursor = self.db.cursor()
            else:
                print('Connection failed.')
        except Exception as e:
            print('Connection error -> {!s}'. format(e))

    def query(self, query_msg):
        """
            Run a query (only works with Select, shows, describe) and return the result set back
            :param str query_msg: The query to run
            :return: Returns "None" if error, otherwise array list
        """
        if not self.cursor or not self.db.is_connected():
            print("No connection to MariaDB ! Check connection.")
            return None
        try:
            start_time = datetime.now()
            print('Starting query ({!s}): "{!s}" ...'.format(start_time, query_msg))
            self.cursor.execute(query_msg)

            # # Use fetchall() in case of a small table to retrieve all infos at once
            # rows = self.cursor.fetchall()
            # headers_list = [i[0] for i in self.cursor.description]
            # # Print could take time if there is a lots of rows
            # print(tabulate(rows, headers=headers_list, tablefmt='psql'))

            # Display a result at each loops. Avoid to used one print to display all datas which can leads to issue
            # if there is too many datas (Memory issue, freeze script)
            # Set Header list
            headers_list = [i[0] for i in self.cursor.description]
            while True:
                rows = self.cursor.fetchmany(size=200)
                if not rows:
                    break
                # Print could take time if there is a lots of rows
                print(tabulate(rows, headers=headers_list, tablefmt='psql'))

            # Total number of rows
            print('Total Row(s):', self.cursor.rowcount)
            # Query + display spent time
            last_spent_query = datetime.now() - start_time
            print('Query spent {!s} seconds '.format(last_spent_query.total_seconds()))

        except Exception as err:
            print('Query failed -> {!s} !'.format(err))
            return None

    def get_query_result(self, query_msg):
        # TODO Warning with a query de plus de 10000 reponse ça peut merder car la variable va prendre beaucoup de plus
        # en RAM
        """
            Run a query (only works with Select, shows, describe) and return the result set back
            :param str query_msg: The query to run
            :return: Returns "None" if error, otherwise array list
        """
        if not self.cursor or not self.db.is_connected():
            print("No connection to MariaDB ! Check connection.")
            return None
        try:
            start_time = datetime.now()
            print('Starting query ({!s}): "{!s}" ...'.format(start_time, query_msg))
            self.cursor.execute(query_msg)
            return self.cursor.fetchall()

        except Exception as err:
            print('Query failed -> {!s} !'.format(err))
            return None

    def insert_temp(self, table_name, datas_to_insert):
        query = "INSERT IGNORE INTO " + table_name + \
                " (datetime,sondeid,sondename,hostname,temperature) VALUES(%s,%s,%s,%s,%s) "
        try:
            start_time = datetime.now()
            print('Starting query ({!s}): "{!s}" ...'.format(start_time, query))
            self.cursor.executemany(query, datas_to_insert)
            # TODO see if we can not do autocommit()
            self.db.commit()
            print("{!s} records inserted successfully into {!s} table.".format(self.cursor.rowcount, table_name))

        except Exception as exp:
            print('Insert Error for query "{!s}" -> {!s}'.format(query, exp))
            # self.db.rollback()

    def delete_table(self, table_name):
        """
            Delete table content
            :param str table_name: name of the table
            :return: nothing
        """
        try:
            query = 'truncate table ' + table_name + ";"
            self.cursor.execute(query)
            # TRUNCATE causes implicit commit so no need to commit
            print('All records have been deleted successfully from table "{!s}". '.format(table_name))

        except Exception as exp:
            print("Failed to delete all records from table: {!s} ! Error -> {!s}.".format(table_name, exp))

    def close(self):
        if self.db and self.cursor:
            print('Closing connection to database ...')
            self.cursor.close()
            self.db.close()
        else:
            print('No connection established')


if __name__ == '__main__':
    print('Test connection mariadb')

    # load Mysql conn info from env file
    env_path = Path('cfg') / 'settings_test.env'
    load_dotenv(dotenv_path=env_path, verbose=True)

    MYSQL_LOGIN = os.getenv("MYSQL_LOGIN")
    MYSQL_PASSWORD = os.getenv("MYSQL_PASSWORD")
    MYSQL_HOST = os.getenv("MYSQL_HOST")
    MYSQL_PORT = os.getenv("MYSQL_PORT")
    MYSQL_NAME = os.getenv("MYSQL_NAME")
    mysql = Mysql(login=MYSQL_LOGIN, password=MYSQL_PASSWORD,
                  host=MYSQL_HOST, port=MYSQL_PORT, database_name=MYSQL_NAME)
    mysql.login_database()
    mysql.query('describe tempreading;')
    mysql.query('show tables;')
    # # mysql.query('select * from tempreading;')
    # test = mysql.get_query_result('describe tempreading;')
    #
    # mysql.query('select datetime from tempreading where sondename="vitrine-3" order by datetime desc limit 1;')
    # mysql.query('select datetime from tempreading where sondename="ilot-2" order by datetime desc limit 1;')
    #
    # test2=mysql.get_query_result('select datetime from tempreading where sondename="vitrine-3" order by datetime desc limit 1;')
    # test3=mysql.get_query_result('select datetime from tempreading where sondename="ilot-2" order by datetime desc limit 1;')
    #
    # "ilot-2"
    #
    # probe_last_date = test2[0][0]
    # probe_last_date1 = test3[0][0]

    # # Exemple : probe_last_date = 2019-11-02 17:50:00  et probe_last_date1 = 2019-10-28 10:00:00
    # if probe_last_date > probe_last_date1:
    #     print('{!s} est plus récent que {!s}'.format(probe_last_date,
    #                                                  probe_last_date1))

    # test_obj.delete_table('tempreading')
    # mysql.query('select * from tempreading;')
    datas_to_insert = [('2021-07-04 14:09:35.361302', '2898', 'vitrine1', 'thermopi-1', 4.812),
                       ('2021-07-04 16:09:35.361302', '2898', 'vitrine1', 'thermopi-1', 5.23),
                       ('2021-07-04 17:09:35.361302', '2898', 'vitrine1', 'thermopi-1', 10.819),
                       ('2021-07-05 17:10:35.361302', '2898', 'vitrine1', 'thermopi-1', 11.812)
                       ]
    # mysql.insert_temp('tempreading', datas_to_insert)
    # mysql.query('select * from tempreading;')
    mysql.query('select * from tempreading order by datetime desc limit 10;')
    mysql.close()


