#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import os
import argparse
from pathlib import Path
import re
import datetime
import tempfile
import shutil
import json
from typing import List, Tuple

from dotenv import load_dotenv
import mysql.connector as _mysql


class DBConn:
    """ Context manager for MySQL
    """

    def __init__(self, host, port, user, password, database):
        self.conn = _mysql.connect(
            host=host,
            port=port,
            user=user,
            passwd=password,
            database=database
        )
        self.cursor = self.conn.cursor()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        # close cursor + db connection
        self.cursor.close()
        self.conn.close()


def purge_db(db: DBConn, threshold: datetime.date, dry_run: bool = False) -> Tuple[int, List[str]]:

    deleted: int = 0
    deleted_rows: List[str] = []

    if dry_run:

        query = ("SELECT * FROM tempreading "
                 "WHERE datetime < %s ")
        db.cursor.execute(query, (threshold,))
        for res in db.cursor:
            deleted_rows.append(res)
        #    print(f"{res=}")

        query = ("SELECT COUNT(*) FROM tempreading "
                 "WHERE datetime < %s ")
        db.cursor.execute(query, (threshold,))
        delete_count = db.cursor.next()  # only one result here
        # print(f"Should delete {delete_count[0]} entries")
        deleted = delete_count[0]

    else:
        query = ("DELETE FROM tempreading "
                 "WHERE datetime < %s; ")
        # print(f"{query=}")
        db.cursor.execute(query, (threshold,))
        db.conn.commit()
        # print(f"Deleted {db.cursor.rowcount} entries")
        deleted = db.cursor.rowcount

    return deleted, deleted_rows


def purge_file(temperature_path: Path, threshold: datetime.date, dry_run: bool = False) -> Tuple[int, List[str]]:

    deleted: int = 0
    deleted_files: List[str] = []

    # purge csv file
    # folder ~ temperature/2021/[...].txt
    folder_years = [d for d in temperature_path.glob("????") if d.is_dir()]
    # csv_to_delete = []
    for folder_year in folder_years:
        # files ~ 2021-01-05.txt
        rgx = re.compile(r"(%s)-(\d{2})-(\d{2}).txt" % folder_year.name)
        # folder_content = {}
        for f in folder_year.glob("*.txt"):
            rgx_res = rgx.search(f.name)
            if not rgx_res:
                # filename does not match, skipping it
                continue
            f_date = datetime.date(year=int(rgx_res[1]), month=int(rgx_res[2]), day=int(rgx_res[3]))
            # print(f"{f_date=}")
            if f_date < threshold:
                # less than threshold, append for deletion
                deleted_files.append(str(f))

    deleted = len(deleted_files)

    if not dry_run:
        # move files to temp dir then remove folder
        # should avoid partial delete (script canceled or fs error...)
        folder_tmp = tempfile.mkdtemp(prefix="pi_purge", dir=temperature_path)
        for f in deleted_files:
            shutil.move(f, folder_tmp)

        shutil.rmtree(folder_tmp)

    return deleted, deleted_files


def purge(args):
    #
    now = datetime.date.today()
    # print(f"{now=}")
    date_threshold = now - datetime.timedelta(days=args.threshold)
    # print(f"{date_threshold=}")

    # read conf

    if args.test:
        cfg_path = Path("cfg") / "config_test.json"
    else:
        cfg_path = Path("cfg") / "config.json"

    with open(cfg_path) as fp:
        cfg = json.load(fp)

    temperature_path = Path(cfg["probes"]["temperature_dir"])

    # purge csv
    deleted, deleted_files = purge_file(temperature_path, date_threshold, args.dry_run)

    if args.dry_run:
        print(f"Should delete {deleted} files")
        if deleted:
            print("Files to delete:")
            for filename in deleted_files:
                print(f"- {filename}")
    else:
        print(f"Deleted {deleted} files")

    # read db settings

    if args.test:
        env_path = Path('cfg') / 'settings_test.env'
    else:
        env_path = Path('cfg') / 'settings_prod.env'

    print(f"Loading db and mail settings from {env_path}")
    load_dotenv(dotenv_path=env_path, verbose=True)
    mysql_login = os.getenv("MYSQL_LOGIN")
    mysql_password = os.getenv("MYSQL_PASSWORD")
    mysql_host = os.getenv("MYSQL_HOST")
    mysql_port = os.getenv("MYSQL_PORT")
    mysql_name = os.getenv("MYSQL_NAME")

    with DBConn(mysql_host, mysql_port, mysql_login, mysql_password, mysql_name) as db:
        deleted, deleted_rows = purge_db(db, date_threshold, args.dry_run)

    if args.dry_run:
        print(f"Should delete {deleted} entries")
        if deleted:
            print("DB entries to delete:")
            for row in deleted_rows:
                print(f"- {row}")
    else:
        print(f"Deleted {deleted} entries")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--test", action="store_true", help="Load settings_test.env & cfg/config_test.json "
                                                                  "instead of settings.env & cfg/config.json")
    parser.add_argument("-y", "--dry_run", action="store_true", help="Do not delete anything, just print...")
    parser.add_argument("--threshold", type=int,
                        required=True,
                        default=20,
                        help="Remove files and entries in db older than the given number of DAYS")
    args_ = parser.parse_args()
    purge(args_)
