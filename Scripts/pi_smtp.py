#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import os
from pathlib import Path
import smtplib
from smtplib import SMTPAuthenticationError
import base64
from email.message import EmailMessage
from email.base64mime import body_encode as encode_base64

from dotenv import load_dotenv

# TODO adding attachement to e-mail
# TODO return error connection with Smtp.connect() and error logs


class SMTP_SSL2(smtplib.SMTP_SSL):

    def auth_login(self, challenge=None):
        """
        .. note:: code copied/modified from python 3.8 std lib smtplib.py
        """
        # print("auth login", challenge)
        if challenge is None:
            return self.user
        elif challenge == b'Username:':
            return self.user
        elif challenge == b'Password:':
            return self.password

    def auth(self, mechanism, authobject, *, initial_response_ok=True):
        """Authentication command - requires response processing.

        .. note:: code copied/modified from python 3.8 std lib smtplib.py
        """
        # RFC 4954 allows auth methods to provide an initial response.  Not all
        # methods support it.  By definition, if they return something other
        # than None when challenge is None, then they do.  See issue #15014.
        # print("auth", mechanism, authobject)
        mechanism = mechanism.upper()
        initial_response = (authobject() if initial_response_ok else None)
        if initial_response is not None:
            response = encode_base64(initial_response.encode('ascii'), eol='')
            (code, resp) = self.docmd("AUTH", mechanism + " " + response)
        else:
            (code, resp) = self.docmd("AUTH", mechanism)
        # If server responds with a challenge, send the response.
        # if code == 334:
        #     challenge = base64.decodebytes(resp)
        #     response = encode_base64(
        #         authobject(challenge).encode('ascii'), eol='')
        #     (code, resp) = self.docmd(response)
        # If server responds with a challenge, send the response.
        # Note that there may be multiple, sequential challenges.
        while code == 334:
            challenge = base64.decodebytes(resp)
            response = encode_base64(
                authobject(challenge).encode('ascii'), eol='')
            (code, resp) = self.docmd(response)

        if code in (235, 503):
            return (code, resp)
        raise SMTPAuthenticationError(code, resp)


# ######################################################################
class Smtp:
    def __init__(self, smtp_address, port, user, password):
        """
            Init Smtp object with connection infos
            :param str smtp_address: address of the remote smtp server
            :param str port: port of the remote smtp server
            :param str user: user used to connect to smtp server
            :param str password: password used to connect to smtp server
        """
        self.__smtp_address = smtp_address
        self.__port = port
        self.__user = user
        self.__password = password
        self.server_smtp = None

    def connect_server_smtp(self):
        """
            Function used to connect to smtp server with configured infos into smtp object
            :return: Nothing
        """
        # try:
        # Init server_smtp with SSL
        # self.server_smtp = smtplib.SMTP_SSL(host=self.__smtp_address)  # SSL
        self.server_smtp = SMTP_SSL2(host=self.__smtp_address, port=self.__port)
        # Active debug log
        self.server_smtp.set_debuglevel(2)
        # Connect to remte smtp server
        # self.server_smtp.connect(host=self.__smtp_address, port=self.__port)
        #  Check connection with server smtp is OK -> smtp hello
        # self.server_smtp.ehlo()
        # Authentication
        self.server_smtp.login(self.__user, self.__password, initial_response_ok=True)

        # except Exception as exp:
        #     print('Connection to server smtp failed -> {!s}'. format(exp))
        #     self.server_smtp = None

    def close(self):
        """
            Close connection with remote smstp server
            :return: Nothing
        """
        if self.server_smtp:
            print('Closing connection with server smtp ...')
            self.server_smtp.quit()
        else:
            print('No connection established with server smtp')

    def send_email(self, from_addr, to_addr, subject, message, to_bcc=None):
        """
            Send e-mail through smtp server
            :param str from_addr: sender e-mail address
            :param list to_addr: a list of addresses to send this mail to
            :param str subject: e-mail subject
            :param str message: e-mail message or content
            :param str to_bcc: email bcc
            :return: Nothing
        """
        # TODO check connection to smtp server is OK first ("if not self.server_smtp" ne suffit pas)
        if not self.server_smtp:
            print('No connection to smtp server established ! First try to connect to smtp server.')
            return

        # Build e-mail message
        # msg = """\
        #     From: %s\r\n\
        #     To: %s\r\n\
        #     Subject: %s\r\n\
        #     \r\n\
        #     %s
        #     """ % (from_addr, ", ".join(to_addr), subject, message)

        #try:
        # self.server_smtp.sendmail(from_addr, to_addr, msg)
        #except Exception as exp:
        #    print('Failed to send e-mail! Cause -> {!s}'.format(exp))

        msg = EmailMessage()
        msg.set_content(message)
        msg['Subject'] = subject
        msg['From'] = from_addr
        msg['To'] = to_addr
        #msg['Cc'] = them
        if to_bcc:
            msg['Bcc'] = to_bcc
        self.server_smtp.send_message(msg)


if __name__ == '__main__':
    print('Test smtp ....')

    # load Mysql conn info from env file
    env_path = Path('cfg') / 'settings_test.env'
    load_dotenv(dotenv_path=env_path, verbose=True)

    SMTP_USER = os.getenv("SMTP_USER")
    SMTP_PASSWORD = os.getenv("SMTP_PASSWORD")
    SMTP_HOST = os.getenv("SMTP_HOST")
    SMTP_PORT = os.getenv("SMTP_PORT")
    SMTP_ADDR_FROM = os.getenv("SMTP_ADDR_FROM")
    SMTP_ADDR_TO = os.getenv("SMTP_ADDR_TO").split(",")
    print("SMTP ADDR", SMTP_ADDR_FROM, SMTP_ADDR_TO)
    SMTP_BCC_TO = os.environ["SMTP_BCC_TO"].split(",") if "SMTP_BCC_TO" in os.environ else None

    # Init Smtp object
    smtp_server = Smtp(smtp_address=SMTP_HOST, port=SMTP_PORT,
                       user=SMTP_USER, password=SMTP_PASSWORD)
    # Connect to smtp server
    smtp_server.connect_server_smtp()
    # Send e-mail
    smtp_server.send_email(from_addr=SMTP_ADDR_FROM,
                           to_addr=SMTP_ADDR_TO,
                           subject="test pi envoie e-mail",
                           message="Hello \nComment ca va ?\nCordialement,\nGarfield 1",
                           to_bcc=SMTP_BCC_TO)
    # Close connection with smtp server
    smtp_server.close()



