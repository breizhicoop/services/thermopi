#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import RPi.GPIO as GPIO
from datetime import datetime
import os
import csv
import socket
from glob import glob
from Scripts.parser_config_json import ConfigParser

GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)


def detect_all_probes(probes_hard_path):
    """
        Detect all probes ids configured and up on one raspberry pi.
        :param str probes_hard_path: directory path where we could find probe infos (driver, temperature, etc ...)
        :return list probes_id: None if nothing found or a list with all probes ids on the raspberry
    """
    list_probes_id = list()
    probes_path = glob(probes_hard_path)
    for probe_path in probes_path:
        probe_id = os.path.basename(probe_path)
        list_probes_id.append(probe_id)
    return list_probes_id


class ProbeTemp:

    def __init__(self, probe_id, probe_name, device_path, save_path_temperature=None):
        self.probe_id = probe_id
        self.probe_name = probe_name
        self.device_path = device_path
        self.current_temperature = None
        self.current_temp_timestamp = None
        self.save_path_temperature = save_path_temperature

    def get_temp(self):
        try:
            with open(self.device_path) as temp_file:
                content = temp_file.read()
        except Exception as exp:
            print('Can not read and extra temperature info from "{!s}" !'.format(self.device_path))

        self.current_temp_timestamp = datetime.now()
        # Take the last line of content splitlines()[-1]
        # Split the last line with separator 't=' and get the last infos "[-1]"
        temperature_data = content.splitlines()[-1].split('t=')[-1]
        self.current_temperature = float(temperature_data) / 1000
        print('Current temperature for probe "{!s}" id "{!s}" is : {!s} °C.'.
              format(self.probe_name, self.probe_id, self.current_temperature))

        return self.current_temperature

    def save_current_temperature(self, save_temp_full_path=None):

        if save_temp_full_path:
            temp_logs = save_temp_full_path
        else:
            # By default , temperature will be save under path <save_path_temperature>/<current_years>/< YY-MM-DD.txt>
            # You can change only <save_path_temperature>
            temp_logs = os.path.join(self.save_path_temperature,
                                     str(self.current_temp_timestamp.year),
                                     str(self.current_temp_timestamp.date()) + '.txt'
                                     )

        directory = os.path.dirname(temp_logs)
        if not os.path.isdir(directory):
            os.makedirs(directory)

        # CSV Configuration.
        headers = ['datetime', 'sondeid', 'sondename', 'hostname', 'temperature']
        rows = [str(self.current_temp_timestamp),
                self.probe_id,
                self.probe_name,
                socket.gethostname(),
                self.current_temperature]

        if not os.path.exists(temp_logs):
            with open(temp_logs, 'w+') as csv_file:
                csv_writer = csv.writer(csv_file, delimiter=",")
                csv_writer.writerow(headers)
                csv_writer.writerow(rows)
        else:
            with open(temp_logs, 'a+') as csv_file:
                csv_writer = csv.writer(csv_file, delimiter=",")
                csv_writer.writerow(rows)

    def get_temp_and_save(self):
        self.get_temp()
        self.save_current_temperature()


if __name__ == '__main__':
    # Get config from parser
    all_config = ConfigParser("/home/pi/PycharmProjects/breizhasso/cfg/config.json")
    # Update configuration infos with only existing probes.
    all_config.update_probe_spec()

    # TODO verification que l'on a bien des specifications.
    for probe_spec in all_config.probes.get("specifications"):
        # For each detected probe get temperature
        probe_temp = ProbeTemp(probe_spec.get("probe_id", "Probe_id not found"),
                               probe_spec.get("probe_name", "Probe_name not found"),
                               probe_spec.get("device_path", "Device_path not found"),
                               save_path_temperature=all_config.probes.get("save_path_temperature"))
        # Get probe temp and save its temperature into a file
        probe_temp.get_temp_and_save()




