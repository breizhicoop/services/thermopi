#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import os
from pathlib import Path

from dotenv import load_dotenv

# ENV
# Local path is path where settings is located
LOCAL_PATH = os.path.dirname(__file__)
WORK_PATH = os.path.dirname(LOCAL_PATH)
CONFIG_NAME = "config.json"
DEFAULT_DEVICE_CONFIG_PATH = os.path.join(LOCAL_PATH, CONFIG_NAME)

# DEBUG_LOGS_NAME = "temp_readings.log"
# DEBUG_LOGS_PATH = os.path.join(LOCAL_PATH, "../logs", DEBUG_LOGS_NAME)

# load Mysql conn info from env file
env_path = Path(LOCAL_PATH) / 'settings_prod.env'
load_dotenv(dotenv_path=env_path, verbose=True)

MYSQL_LOGIN = os.getenv("MYSQL_LOGIN")
MYSQL_PASSWORD = os.getenv("MYSQL_PASSWORD")
MYSQL_HOST = os.getenv("MYSQL_HOST")
MYSQL_PORT = os.getenv("MYSQL_PORT")
MYSQL_NAME = os.getenv("MYSQL_NAME")
