import os
from pathlib import Path

from dotenv import load_dotenv
from fabric import task, Connection

# code adapted from https://github.com/yoongkang/fabric-deployment/blob/master/fabfile.py


def get_hosts(prod=False, test=True):
    """
    Returns a list of dictionaries which can be passed as keyword arguments
    to instantiate a `Connection`.
    Example:
        >>> hosts = get_hosts()
        >>> c = Connection(**hosts[0])
    """

    # load conn info from env file
    if test:
        env_path = Path("cfg") / "settings_deploy_test.env"
    elif prod:
        env_path = Path("cfg") / "settings_deploy.env"
    else:
        raise RuntimeError("Need to specify: --test or --prod")

    print("Loading env:", env_path)
    load_dotenv(dotenv_path=env_path, verbose=True)

    # env.user = "root"
    env_hosts = os.getenv("DEPLOY_HOSTS").split(",")
    env_password = os.getenv("DEPLOY_PWD")
    print("deploy hosts", env_hosts)

    return [{
        "host": host,
        "connect_kwargs": {"password": env_password},
    } for host in env_hosts]


@task
def host_deploy(c, test=False):

    folder_dest = Path("/home/pi/git/thermopi2/")

    # copy Scripts/*.py
    p = Path(r"Scripts").glob("*.py")
    pyfiles = [x for x in p if x.is_file()]
    # print("pyfiles", pyfiles)

    folder_scripts = folder_dest / "Scripts"
    c.run(f"mkdir -p {folder_scripts}")
    for pyfile in pyfiles:
        print(f"Sending [file] {pyfile} to: {folder_scripts}")
        c.put(pyfile, str(folder_scripts))

    # copy cfg/*
    folder_cfg = folder_dest / "cfg"
    c.run(f"mkdir -p {folder_cfg}")
    print(f"Sending [cfg] cfg/config.json to: {folder_cfg}")
    c.put("cfg/config.json", str(folder_cfg))
    print(f"Sending [cfg] cfg/settings.py to: {folder_cfg}")
    c.put("cfg/settings.py", str(folder_cfg))
    if test:
        env_filepath = "cfg/settings_test.env"
    else:
        env_filepath = "cfg/settings_prod.env"
    print(f"Sending [env] {env_filepath} to: {folder_cfg}")
    c.put(env_filepath, str(folder_cfg))
    print(f"Sending [env] cfg/settings_prod.env to: {folder_cfg}")
    c.put("cfg/settings_prod.env", str(folder_cfg))

    # copy requirements.txt
    print(f"Sending [file] requirements.txt to: {folder_dest}")
    c.put("requirements.txt", str(folder_dest))

    # c.run("sudo apt update")
    # c.run("sudo apt install -y python3-venv")
    # c.run("sudo apt install -y python3-pip")

    # create venv if it does not already exists
    if c.run("{} --version".format(folder_dest / "venv/bin/python"), warn=True).failed:
        with c.cd(str(folder_dest)):
            cmd_venv_create = "python3 -m venv venv"
            print("Creating virtual env", cmd_venv_create)
            c.run(cmd_venv_create)

    # upgrade pip
    with c.cd(str(folder_dest)):
        cmd_pip_upgrade = "{} install --upgrade pip".format("venv/bin/python -m pip")
        print("Upgrading pip venv:", cmd_pip_upgrade)
        c.run(cmd_pip_upgrade)

    # update venv
    cmd_pip_up = "{} install -r {} --upgrade".format("venv/bin/python -m pip", folder_dest / "requirements.txt")
    with c.cd(str(folder_dest)):
        print("Updating venv:", cmd_pip_up)
        c.run(cmd_pip_up)


@task
def deploy(prod=False, test=False):
    """
    Main command line task to deploy
    """

    print("Start deploying....")

    for index, host in enumerate(get_hosts(prod=prod, test=test)):
        print(f"****** Deploying to host {index} at {host['host']} ******")
        remote = Connection(**host)
        host_deploy(
            remote, test
        )

    print("Done.")
