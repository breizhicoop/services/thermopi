# ThermoPi

# ThermoPi Dev

Dumping db schema:

mysqldump -u DB_LOGIN -h DB_HOST -P DB_PORT -p --no-data DB_NAME > schema.sql

Note: please strip any info about os/version/hostname...

# Docker

## Build the DockerFile

* docker build -t thermopi_db .
* docker build -t thermopi_db --no-cache .

## Start container

* docker run -d --rm --name thermopi -p3306:3306 thermopi_db
* docker run --rm --name thermopi -p3306:3306 thermopi_db

## Get a shell in docker 

* docker exec -it thermopi /bin/bash

## Connect to server

* mysql --host 127.0.0.1 --port 3306 --database thermopi --user thermopi --password=thermopi
* mysql --host 127.0.0.1 --port 3306 --database thermopi --user thermopi --password=thermopi < tests/data/tempreading_data_basic.sql

## Stop container

* docker stop thermopi

# MariaDB

* SHOW TABLES;
* SELECT * FROM tempreading;

