-- MySQL dump
--
-- Database: thermopi
-- ------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tempreading`
--

DROP TABLE IF EXISTS `tempreading`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tempreading` (
  `datetime` datetime NOT NULL,
  `sondeid` varchar(32) NOT NULL,
  `sondename` varchar(64) NOT NULL,
  `hostname` varchar(64) NOT NULL,
  `temperature` decimal(4,2) NOT NULL,
  PRIMARY KEY (`datetime`,`sondeid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `temp_alerte`
--

DROP TABLE IF EXISTS `temp_alerte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temp_alerte` (
  `sondename` varchar(64) NOT NULL,
  `alert_temperature` BOOLEAN NOT NULL DEFAULT FALSE,
  `alert_working` BOOLEAN NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`sondename`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES temp_alerte WRITE;
INSERT INTO temp_alerte (sondename) VALUES ('Local-2_Vitrine-1'),('Local-2_Vitrine-2'),
('Local-2_Vitrine-3'),('Local-1'),('Local-2'),('Local-2_congelateur_coffre'),
('Local-2_congelateur_porte'),('Extra'),('Reserve_ChambreFroide-FLEG'),('Reserve_ChambreFroide-Frais'),('Reserve');
UNLOCK TABLES;


/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

