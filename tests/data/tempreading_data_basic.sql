LOCK TABLES tempreading WRITE;
INSERT INTO tempreading (datetime, sondeid, sondename, hostname, temperature) VALUES
    ("2020-07-08 15:00:11", "28-01145364dfaa", "Local-2_Vitrine-3         ", "ThermoPi-2", " 2.56"),
    ("2020-07-08 15:10:03", "28-0114536e5baa", "Local-2_Vitrine-2         ", "ThermoPi-2", " 2.38"),
    ("2020-07-08 15:10:04", "28-01145364dfaa", "Local-2_Vitrine-3         ", "ThermoPi-2", " 2.19"),
    ("2020-07-08 15:20:02", "28-0114536e5baa", "Local-2_Vitrine-2         ", "ThermoPi-2", " 2.31"),
    ("2020-07-08 15:20:03", "28-01145364dfaa", "Local-2_Vitrine-3         ", "ThermoPi-2", " 3.50"),
    ("2020-07-08 15:20:04", "28-0114536d6eaa", "Local-2_Vitrine-1         ", "ThermoPi-2", " 3.25"),
    ("2020-07-08 15:20:04", "28-01145429d8aa", "Local-1                   ", "ThermoPi-4", "21.38"),
    ("2020-07-08 15:20:05", "28-0114536544aa", "Local-2                   ", "ThermoPi-4", "22.69"),
    ("2020-07-08 15:30:02", "28-0114536661aa", "Local-2_dessus_congelateur", "ThermoPi-3", "21.94"),
    ("2020-07-08 15:30:02", "28-0114536e5baa", "Local-2_Vitrine-2         ", "ThermoPi-2", " 2.44");
UNLOCK TABLES;
